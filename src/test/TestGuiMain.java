package test;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TestGuiMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        // modify the window
        primaryStage.setTitle("DBMS Simulator V2018.1");

        // working with root
        Parent root = FXMLLoader.load(getClass().getResource("../main/java/gui/fxsource/start.fxml"));

        // show something in window
        Scene scene = new Scene(root);

        // working again with window
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
