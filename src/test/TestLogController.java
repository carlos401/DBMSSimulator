package test;

import main.java.core.query.QueryType;
import main.java.core.statistics.AverageStatistics;
import main.java.core.statistics.CurrentStatistics;
import main.java.core.statistics.FinalStatistics;
import main.java.log.LogController;

import java.io.IOException;

public class TestLogController {
    public static void main(String[] args) throws IOException {

        /*CurrentStatistics cs= new CurrentStatistics();
        cs.setClock(2.1);
        cs.updateLenghtPerModule("s",25);
        cs.updateLenghtPerModule("i",10);

        cs.increaseDeletedConnections();
        cs.increaseRejectedConnections();

        CurrentStatistics cs2= new CurrentStatistics();
        cs2.setClock(30.0);
        cs2.updateLenghtPerModule("q",100);
        cs2.updateLenghtPerModule("y",65);

        cs2.increaseDeletedConnections();
        cs2.increaseDeletedConnections();
        cs2.increaseRejectedConnections();
        cs2.increaseRejectedConnections();*/

        LogController lc = new LogController();
        /*lc.logCurrentStatistics(cs);
        lc.logCurrentStatistics(cs2);*/

        FinalStatistics fs = new FinalStatistics() ;
        fs.updateAverageLengthPerModule("a",10);
        fs.updateAverageLengthPerModule("b",9);
        fs.updateAverageLengthPerModule("c",5);
        fs.updateAverageLengthPerModule("c",4);

        fs.getAverageLengthPerModule();

        fs.updateAverageTimePerQueryTypePerModule("a", QueryType.JOIN, 10.0);
        fs.updateAverageTimePerQueryTypePerModule("a", QueryType.DDL, 20.0);
        fs.updateAverageTimePerQueryTypePerModule("a", QueryType.SELECT, 40.0);
        fs.updateAverageTimePerQueryTypePerModule("a", QueryType.JOIN, 20.0);
        fs.updateAverageTimePerQueryTypePerModule("b", QueryType.UPDATE, 100.0);
        fs.updateAverageTimePerQueryTypePerModule("b", QueryType.UPDATE, 20.0);
        fs.updateAverageTimePerQueryTypePerModule("b", QueryType.JOIN, 400.0);
        fs.updateAverageTimePerQueryTypePerModule("b", QueryType.JOIN, 220.0);
        fs.getAverageTimePerQueryTypePerModule();

        fs.increaseDeletedConnections();
        fs.increaseDeletedConnections();
        fs.increaseNumberOfQueries();
        fs.increaseRejectedConnections();

        FinalStatistics ww = new FinalStatistics() ;
        ww.updateAverageLengthPerModule("a",10);
        ww.updateAverageLengthPerModule("b",9);
        ww.updateAverageLengthPerModule("d",5);
        ww.updateAverageLengthPerModule("d",4);

        AverageStatistics as = new AverageStatistics();
        as.updateFinalAverageLengthPerModuleJose(fs);
        as.updateFinalAverageLengthPerModuleJose(ww);
        as.fullAverageLengthPerModule();
        as.fullRejectedConnections();
        as.fullDeletedConnections();
        as.fullTotalDiscarted();


        lc.logFinalStatistics(fs);
        lc.logAverageStatistics(as);





    }
}
