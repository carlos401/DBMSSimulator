/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 */

package main.java.gui.controllers;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class controls the Start scene
 */
public class StartController implements Initializable {

    @FXML
    private Button btnNewSimulation;

    /**
     * Gives control to configuration scene
     * @param event the main.java.gui event
     * @throws IOException
     */
    @FXML
    public void newSimulationButtonPushed(ActionEvent event) throws IOException
    {
        //load the configuration scene file
        Parent root = FXMLLoader.load(getClass().getResource("../fxsource/configuration.fxml"));
        Scene scene = new Scene(root);
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        // put the scene
        window.setScene(scene);
        window.show();
    }

    @FXML
    private Button btnCloseApplication;

    /**
     * Closes the app
     * @param event the main.java.gui event
     */
    @FXML
    public void closeApplicationButtonPushed(ActionEvent event){
        System.exit(0);
    }

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //TODO
    }

}
