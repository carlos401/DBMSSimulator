package main.java.gui.controllers;

import javafx.event.ActionEvent;
import javafx.event.Event;
import main.java.core.simulator.Simulator;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class ResultsController implements Initializable{

    @FXML
    private MenuItem menuItemCloseApp;

    @FXML
    public void menuItemCloseAppPushed(ActionEvent event) {
        simulatorThread.interrupt();
        System.exit(0);
    }

    @FXML
    private Label lblNumSim;

    @FXML
    private Label lblDuracSim;

    @FXML
    private Label lblModo;

    @FXML
    private Label lblK;

    @FXML
    private Label lblP;

    @FXML
    private Label lblN;

    @FXML
    private Label lblM;

    @FXML
    private Label lblT;

    /**
     * Allows initialize the simulator and get currentStatistics
     * @param simulator the new simulator to evaluate
     */
    public void initData(Simulator simulator){
        //read the data and put that in main.java.gui
        this.lblNumSim.setText(Integer.toString(simulator.getNumberOfRepetitions()));
        this.lblDuracSim.setText(Integer.toString(simulator.getTimePerSimulation()));
        this.lblModo.setText(simulator.getSlowModeString());
        this.lblK.setText(Integer.toString(simulator.getK()));
        this.lblP.setText(Integer.toString(simulator.getP()));
        this.lblN.setText(Integer.toString(simulator.getN()));
        this.lblM.setText(Integer.toString(simulator.getM()));
        this.lblT.setText(Integer.toString(simulator.getT()));
        this.simulator = simulator;
    }

    @FXML
    private MenuItem startAppBtn;

    private Simulator simulator;

    private Thread simulatorThread;

    @FXML
    public void startAppPushed(ActionEvent event){
        this.simulator.setResultController(this);
        simulatorThread = new Thread(simulator);
        simulatorThread.start();
    }

    @FXML
    private TextArea console;

    /**
     * Allows print in console main.java.gui some text
     * @param text the text to be printed
     */
    @FXML
    public void printInConsole(String text){
        this.console.insertText(console.getLength(),text+"\n");
    }

    @FXML
    private BarChart<String,Number> queueLenghtPerModule;

    @FXML
    private CategoryAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
    public void updateQueueLenghtPerModuleBarChart(XYChart.Series<String,Number> data){
        queueLenghtPerModule.setData(FXCollections.<XYChart.Series<String,Number>>observableArrayList(data));
    }

    @FXML
    private BarChart<String,Number> timePerQueryPerModule;

    @FXML
    private CategoryAxis xAxist;

    @FXML
    private NumberAxis yAxist;

    @FXML
    public void updatetimePerQueryPerModuleBarChart(XYChart.Series<String,Number> data){
        this.timePerQueryPerModule.getData().add(data);
    }

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
