package main.java.gui.controllers;
import main.java.core.simulator.Simulator;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Popup;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class ConfigurationController implements Initializable{

    @FXML
    private Button btnCancel;

    /**
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void cancelButtonPushed(ActionEvent event) throws IOException
    {
        Parent root = FXMLLoader.load(getClass().getResource("../fxsource/start.fxml"));
        Scene scene = new Scene(root);

        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        //
        window.setScene(scene);
        window.show();
    }

    @FXML
    private TextField textFieldNumberOfSimulations;

    @FXML
    private TextField textFieldTimePerSimulation;

    @FXML
    private CheckBox checkBoxDelay;

    @FXML
    private TextField textFieldK;

    @FXML
    private TextField textFieldP;

    @FXML
    private TextField textFieldT;

    @FXML
    private TextField textFieldM;

    @FXML
    private TextField textFieldN;

    @FXML
    private Button btnStart;

    private boolean parametersGiven(){
        return
                checkField(this.textFieldNumberOfSimulations) &&
                        checkField(this.textFieldTimePerSimulation) &&
                        checkField(this.textFieldK) &&
                        checkField(this.textFieldT) &&
                        checkField(this.textFieldN) &&
                        checkField(this.textFieldP) &&
                        checkField(this.textFieldM);

    }

    private boolean checkField(TextField textField){
        String textFieldString = textField.getText();
        try{
            return
                    !textFieldString.isEmpty() &&
                            Integer.parseInt(textFieldString)>=0;
        } catch (Exception e){
            return false;
        }
    }

    private int textFieldToInt(TextField textField){
        return Integer.parseInt(textField.getText());
    }

    private static Popup createPopup(final String message) {
        final Popup popup = new Popup();
        popup.setAutoFix(true);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        Label label = new Label(message);
        label.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                popup.hide();
            }
        });
        label.getStylesheets().add("main/java/gui/stylesheets/modena_dark.css");
        label.getStyleClass().add("popup");
        popup.getContent().add(label);
        return popup;
    }

    private static void showPopupMessage(final String message, final Stage stage) {
        final Popup popup = createPopup(message);
        popup.setOnShown(e -> {
            popup.setX(stage.getX() + stage.getWidth()/2 - popup.getWidth()/2);
            popup.setY(stage.getY() + stage.getHeight()-33 - popup.getHeight()-33);
        });
        popup.show(stage);
    }

    @FXML
    public void startButtonPushed(ActionEvent event)throws IOException{
        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        if (this.parametersGiven()){
            Simulator simulator = new Simulator(
                    textFieldToInt(this.textFieldNumberOfSimulations),
                    textFieldToInt(this.textFieldTimePerSimulation),
                    this.checkBoxDelay.isSelected(),
                    textFieldToInt(this.textFieldK),
                    textFieldToInt(this.textFieldT),
                    textFieldToInt(this.textFieldN),
                    textFieldToInt(this.textFieldP),
                    textFieldToInt(this.textFieldM));

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../fxsource/results.fxml"));

            Parent root = loader.load();
            Scene scene = new Scene(root);

            //access the controller and call a method
            ResultsController controller = loader.getController();
            controller.initData(simulator);
            window.setScene(scene);
            window.show();
        }
        // there is a problem with the parameters
        else {
            showPopupMessage("¡Revisa con atención la configuración introducida!",window);
        }
    }

    @FXML
    private Button btnHelp;

    //to manage help file
    private final String HTML_PATH = "/src/main/java/gui/files/help.html";

    private String getHelpManualPath(){
        String path = Paths.get(".").toAbsolutePath().normalize().toString().replaceAll("\\\\","/");
        return path + HTML_PATH;
    }

    @FXML
    public void helpButtonPushed(ActionEvent event){
        String os = System.getProperty("os.name").toLowerCase();
        switch (os){
            case "linux":
                Runtime runtime = Runtime.getRuntime();
                try {
                    runtime.exec("xdg-open " + getHelpManualPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "windows 10":
                try{
                    URI u = new URI(getHelpManualPath());
                    java.awt.Desktop.getDesktop().browse(u);
                } catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case "mac os":
                try{
                    Runtime rt = Runtime.getRuntime();
                    rt.exec("open " + getHelpManualPath());
                } catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }

    @FXML
    private MenuItem menuItemCloseApp;

    @FXML
    public void menuItemCloseAppPushed(javafx.event.ActionEvent event) {
        System.exit(0);
    }

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //TODO
    }
}
