/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 *
 * @author JOSE RODOLFO VALVERDE JARA
 */

package main.java.log;

import main.java.core.query.QueryType;
import main.java.core.statistics.AverageStatistics;
import main.java.core.statistics.CurrentStatistics;
import main.java.core.statistics.FinalStatistics;

import java.io.IOException;
import java.util.HashMap;


/**
 * Log Controller
 */
public class LogController {

    //instance of log
    private Log log;

    /**
     * LogController constructor.
     * Initialize a log instance to create folder and file in system.
     */
    public LogController() {
        log = new Log();
    }

    /**
     * Log for current status currentStatistics with clock, Module, length per module, deleted and rejected connections
     * @param currentStatistics
     * @throws IOException
     */
    public void logCurrentStatistics(CurrentStatistics currentStatistics) throws IOException {

        String currentFile = log.getFileName();
        log.InsertLog(currentFile, "******************************CS**************************************");
        log.InsertLog(currentFile, "Reloj:                 " + currentStatistics.getClock() + "");
        for (String key : currentStatistics.getLengthPerModule().keySet()) {
            log.InsertLog(currentFile, "Modulo:                " + key + " : " + currentStatistics.getLengthPerModule().get(key).toString() + "");
        }
        log.InsertLog(currentFile, "Conexiones eliminadas: " + currentStatistics.getDeletedConnections());
        log.InsertLog(currentFile, "Conexiones rechazadas: " + currentStatistics.getRejectedConnections());
        log.InsertLog(currentFile, "Total descartadas:     " + (currentStatistics.getDeletedConnections() + currentStatistics.getRejectedConnections()));

    }


    public void logFinalStatistics(FinalStatistics finalStatistics) throws IOException {

        String currentFile = log.getFileName();

        log.InsertLog(currentFile, "******************************FS**************************************");
        for (String key : finalStatistics.getAverageLengthPerModule().keySet()) {
            log.InsertLog(currentFile, "Modulo:                " + key + " : " + finalStatistics.getAverageLengthPerModule().get(key).toString() + "");
        }

        //log.InsertLog(currentFile, "Tiempo promedio de vida(Query): " + finalStatistics.getAverageTimeOfQuery());

        log.InsertLog(currentFile, "Conexiones eliminadas: " + finalStatistics.getDeletedConnections());
        log.InsertLog(currentFile, "Conexiones rechazadas: " + finalStatistics.getRejectedConnections());
        log.InsertLog(currentFile, "Total descartadas:     " + (finalStatistics.getDeletedConnections() + finalStatistics.getRejectedConnections()));

        for (String key1 : finalStatistics.getAverageTimePerQueryTypePerModule().keySet()) {
            log.InsertLog(currentFile, "Modulo:                " + key1 + "            ");
            HashMap<QueryType, Double> hash = finalStatistics.getAverageTimePerQueryTypePerModule().get(key1);
            for (QueryType key2 : hash.keySet()) {
                log.InsertLog(currentFile, "Tipo de query:                " + key2.toString() + " : " + hash.get(key2).toString() + "");
            }
        }
    }

    public void logAverageStatistics(AverageStatistics averageStatistics) throws  IOException{

        String currentFile = log.getFileName();
        log.InsertLog(currentFile, "********************************AS************************************");
        for (String key : averageStatistics.fullAverageLengthPerModule().keySet()) {
            log.InsertLog(currentFile, "Modulo:                " + key + " : " + averageStatistics.fullAverageLengthPerModule().get(key).toString() + "");
        }

        log.InsertLog(currentFile, "Conexiones eliminadas: " + averageStatistics.fullDeletedConnections());
        log.InsertLog(currentFile, "Conexiones rechazadas: " + averageStatistics.fullRejectedConnections());
        log.InsertLog(currentFile, "Total descartadas:     " + averageStatistics.fullTotalDiscarted());





    }


}
