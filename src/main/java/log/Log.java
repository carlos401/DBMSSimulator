/*
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACIÓN E INFORMÁTICA
 * @aAUTHOR JOSE RODOLFO VALVERDE JARA.
 */
package main.java.log;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Log class to generate folder, files and inputs to log files in system folder.
 */
public class Log {

    //path for log files.
    private String path = System.getProperty("user.home") + File.separator + "DBMSSIMULATOR" + File.separator + "log";
    //file to create folder.
    private File file;
    //store file name saved in path.
    private String fileName;

    /**
     * Constructor
     */
    public Log() {
        CreateLogFolder();
        this.fileName = CreateLogFile();
    }

    /**
     * This method creates the folder that will include all the Log files.
     */
    public boolean CreateLogFolder() {
        try {
            file = new File(path);

            if (file.exists()) {
                System.out.println(file + " ya existe.");
                return true;
            } else if (file.mkdirs()) {
                System.out.println(file + " se ha creado.");
                return true;
            } else {
                System.out.println(file + " no se pudo crear.");
                return false;
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * Creates log file.
     * @return name of file created in defined path.
     */
    public String CreateLogFile (){
        try {
            String fechaHora = new SimpleDateFormat("dd-MM-yyyy_HHmmss").format(Calendar.getInstance().getTime());
            File log = new File(path + file.separator + "" + fechaHora + ".log");
            log.createNewFile();
            FileOutputStream oFile = new FileOutputStream(log, true);

            try(FileWriter fw = new FileWriter(path + file.separator + "" + fechaHora+".log", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                out.println("************************************************");
                out.println("************************************************");
                out.println("                  "+fechaHora+"                 ");
                out.println("************************************************");
                out.println("******************************************CEJ***");


            } catch (IOException e) {
                //exception handling left as an exercise for the reader
            }

            return fechaHora+".log";

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * This method insert tuple with Log information to current Log file.
     */
    public void InsertLog(String fileName, String message) throws IOException {
        String dateAndTime = new SimpleDateFormat("HH:mm:ss.SSSZ").format(Calendar.getInstance().getTime());

        try(FileWriter fw = new FileWriter(path + file.separator + "" + fileName, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println("["+dateAndTime+"]: "+message);

        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    /**
     * Get file name , from log folder.
     * @return name of file.
     */
    public String getFileName() {
        return fileName;
    }
}
