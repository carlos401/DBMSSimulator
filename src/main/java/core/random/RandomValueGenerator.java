/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author ESTEBAN ORTEGA ACUNA
 */

package main.java.core.random;

import main.java.core.query.QueryType;
import java.util.Random;

public class RandomValueGenerator {

    /**
     * Generates a new random Query type
     * @return a random QueryType
     */
    public static QueryType generateQueryType(){
        int randomQueryType = new Random().nextInt(100);
        if(randomQueryType<=30)
            return QueryType.SELECT;
        else if(randomQueryType <=55)
            return QueryType.UPDATE;
        else if(randomQueryType <=90)
            return QueryType.JOIN;
        else
            return QueryType.DDL;
    }

    /**
     * Generates a new time with normal distribution
     * @param mean represents the average value
     * @param variance
     * @return time with normal distribution
     */
    public static Double generateNormalTime(double mean, double variance){
        Random random = new Random();
        float ri = 0f;
        double x;

        for(int i = 0; i<12; ++i){
            double r = random.nextGaussian();
            ri += r;
        }

        x = mean + (Math.sqrt(variance) * (ri - 6));
        return x;
    }


    /**
     * Generates a new time with uniform distribution
     * @param a
     * @param b
     * @return
     */
    public static Double generateUniformTime(double a, double b){
        double r = new Random().nextDouble();
        return (a +(b-a)) *r;
    }

    /**
     * Generates a new time with exponential distribution
     * @param lambda
     * @return
     */
    public static Double generateExponentialTime(double lambda){
        Random random = new Random();
        float r = random.nextFloat();
            return (-1/lambda) * Math.log(r);
    }
}
