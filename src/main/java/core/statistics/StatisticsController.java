/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.statistics;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.chart.XYChart;
import main.java.gui.controllers.ResultsController;
import java.util.HashMap;

/**
 * This class provides currentStatistics management
 */
public class StatisticsController {

    //
    private CurrentStatistics currentStatistics;

    //
    private FinalStatistics finalStatistics;

    //
    private AverageStatistics averageStatistics;

    //
    private ResultsController resultsController;

    /**
     * The default constructor
     */
    public StatisticsController(ResultsController resultsController){
        this.resultsController = resultsController;
    }

    /**
     * Allows to update the chart in gui
     */
    private void showFinalStatistics(){
        XYChart.Series<String,Number> dataSeries1 = new XYChart.Series<>();
        dataSeries1.setName("Resultado");

        HashMap<String,Double> statistics = finalStatistics.getAverageLengthPerModule();
        for(String key:statistics.keySet()){
            dataSeries1.getData().add(new XYChart.Data<>(key, statistics.get(key)));
        }
        Platform.runLater(() -> resultsController.updateQueueLenghtPerModuleBarChart(dataSeries1));
    }

    /**
     * Allows to print in console the current currentStatistics
     */
    private void printInConsole() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Reloj actual: " + currentStatistics.getClock() + "\n");
        for (String key : currentStatistics.getLengthPerModule().keySet()) {
            stringBuilder.append("Modulo: " + key + " : " + currentStatistics.getLengthPerModule().get(key).toString() + "\n");
        }
        stringBuilder.append("Conexiones eliminadas: " + currentStatistics.getDeletedConnections() + "\n");
        stringBuilder.append("Conexiones rechazadas: " + currentStatistics.getRejectedConnections() + "\n");
        stringBuilder.append("Total descartadas:     " + (currentStatistics.getDeletedConnections()
                + currentStatistics.getRejectedConnections()) + "\n \n");
        Platform.runLater(() -> resultsController.printInConsole(stringBuilder.toString()));
    }

    /**
     * Allows to send currentStatistics object for printing
     * @param currentStatistics the current currentStatistics
     */
    public synchronized void sendCurrentStatistics(CurrentStatistics currentStatistics){
        this.currentStatistics = currentStatistics;
        this.printInConsole();

    }

    /**
     * Allows to send finalStatistics object for printing
     * @param finalStatistics the final currentStatistics
     */
    public void sendFinalStatistics(FinalStatistics finalStatistics){
        this.finalStatistics = finalStatistics;
        this.showFinalStatistics();
    }
}