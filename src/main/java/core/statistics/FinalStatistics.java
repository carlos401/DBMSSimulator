/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.statistics;

import main.java.core.query.QueryType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class provides currentStatistics for each simulation
 */
public class FinalStatistics {

    // Number of queries per module
    private HashMap<String,List<Integer>> averageLengthPerModule;

    // rejected connections
    private int rejectedConnections;

    // deleted connections
    private int deletedConnections;

    // Number of queries per type per module
    private HashMap<String,HashMap<QueryType,List<Double>>> averageTimePerQueryTypePerModule;


    // number of queries
    private int numberOfQueries;

    // lambda for little's law
    private double lambda;

    /**
     * Default constructor
     */
    public FinalStatistics(){
        this.averageLengthPerModule = new HashMap<>();
        this.rejectedConnections = 0;
        this.deletedConnections = 0;
        this.averageTimePerQueryTypePerModule = new HashMap<>();
        this.numberOfQueries = 0;
        this.lambda = 0.5;
    }

    /**
     * Calculate average length per module from a hashMap of <String, Double>
     * @return a hashMap with data from modules.
     */
    public HashMap<String, Double> getAverageLengthPerModule( ) {
       HashMap<String, Double> s= new HashMap<>();
       for (String key: this.averageLengthPerModule.keySet()){
            List<Integer> l = this.averageLengthPerModule.get(key);
            int totalValue=0;
            for (int i = 0; i < l.size(); i++) {
                totalValue= totalValue + l.get(i);
            }
            int averageTime = totalValue / l.size();
            //duda de como tratar el lambda.
            double L = this.lambda * averageTime;
            s.put(key, L);
        }
        return s;
    }

    /**
     * Set Length per module
     * @param module current module
     * @param length length of module.
     */
    public void updateAverageLengthPerModule(String module, Integer length){
        List<Integer> l = this.averageLengthPerModule.get(module);
        if(l == null)
        {
            List<Integer> s = new ArrayList<>();
            s.add(length);
            this.averageLengthPerModule.put(module, s);
        }else{
            l.add(length);
        }
    }

    /**
     * get rejected Connections
     * @return
     */
    public int getRejectedConnections() {
        return rejectedConnections;
    }

    /**
     * set rejected connections increasing by one previos value.
     */
    public void increaseRejectedConnections() {
        this.rejectedConnections++;
    }

    /**
     * Return deleted connections from module
     * @return deleted Connections
     */
    public int getDeletedConnections() {
        return deletedConnections;
    }

    /**
     * Increase by one previos value of deletedConnections
     */
    public void increaseDeletedConnections() {
        this.deletedConnections++;
    }

    /**
     * With inputs like module, querytype and value from that query type, we'll calculate
     * averageTimePerModule in a hashMap of hashMaps.
     * @param module module to input
     * @param queryType queryType to input
     * @param pValue value from querytype
     */
    public void updateAverageTimePerQueryTypePerModule(String module, QueryType queryType, Double pValue) {
        HashMap<QueryType, List<Double>> h = this.averageTimePerQueryTypePerModule.get(module);

        if (!this.averageTimePerQueryTypePerModule.containsKey(module))
        {
                HashMap<QueryType, List<Double>> hm = new HashMap<>();
                List<Double> qv= new ArrayList<>();
                qv.add(pValue);
                hm.put(queryType, qv);
                this.averageTimePerQueryTypePerModule.put(module, hm );
        }else{
            if (h.containsKey(queryType))
            {
                h.get(queryType).add(pValue);
            }else{
                List<Double> qv= new ArrayList<>();
                qv.add(pValue);
                h.put(queryType,qv);
            }
        }
    }
    /**
     * Execute calculation to return average time per query per module of the data set in
     * updataAverageTimePerQueryPerModule
     * @return hashMap with data from modules.
     */
    public HashMap<String, HashMap<QueryType, Double>> getAverageTimePerQueryTypePerModule() {
        HashMap<String ,HashMap<QueryType, Double>> s = new HashMap<>();

        for (String key: this.averageTimePerQueryTypePerModule.keySet())
        {
            HashMap<QueryType, List<Double>> l = this.averageTimePerQueryTypePerModule.get(key);

            HashMap<QueryType, Double> io = new HashMap<>();
            for (QueryType query: l.keySet()) {

                List<Double> ly = l.get(query);
                double totalValue=0;
                for (int i = 0; i < ly.size() ; i++) {
                    totalValue = totalValue + ly.get(i);
                }
                double averageTime = totalValue / ly.size();


                io.put(query,averageTime);
                s.put(key,io);
            }

        }

        return s;
    }

    /**
     * Increase by one previous value of number of queries.
     */
    public void increaseNumberOfQueries() {
        this.numberOfQueries++;
    }
}
