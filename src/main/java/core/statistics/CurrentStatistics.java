/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.statistics;

import java.util.HashMap;

/**
 * This class provides currentStatistics for each clock
 */
public class CurrentStatistics {

    //Clock
    private double clock;

    //Module and length hashMap
    private HashMap<String,Integer> lengthPerModule;

    // rejected connections because of size of buffer
    private int rejectedConnections;

    // deleted connections because of timeout.
    private int deletedConnections;

    /**
     * Current Statistics constructor.
     */
    public CurrentStatistics(){
        this.clock = 0;
        this.lengthPerModule = new HashMap<>();
        this.rejectedConnections = 0;
        this.deletedConnections = 0;
    }

    /**
     * Get clock from currentStatistics
     * @return clock value
     */
    public double getClock() {
        return clock;
    }

    /**
     * Set clock for currentStatistics
     * @param clock double clock value
     */
    public void setClock(double clock) {
        this.clock = clock;
    }

    /**
     * Get rejectedConnections from currentStatistics
     * @return int rejectedConnections
     */
    public int getRejectedConnections() {
        return rejectedConnections;
    }

    /**
     * Set rejectedConnections adding 1 to current value.
     */
    public void increaseRejectedConnections() {
        this.rejectedConnections++;
    }

    /**
     * Get deletedConnections from currentStatistics
     * @return int deletedConnections
     */
    public int getDeletedConnections() {
        return deletedConnections;
    }

    /**
     * Set deletedConnections adding 1 to current value.
     */
    public void increaseDeletedConnections() {
        this.deletedConnections++;
    }

    /**
     * HashMap set with Module and LengthPerModule
     * @return HashMap with LengthPerModule data.
     */
    public HashMap<String, Integer> getLengthPerModule() {
        return lengthPerModule;
    }

    /**
     * Set Module and length value.
     * @param module Module from Simulation.
     * @param lenght Length for Module.
     */
    public void updateLenghtPerModule(String module, Integer lenght){
        this.lengthPerModule.put(module,lenght);
    }
}
