/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 *
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */
package main.java.core.statistics;

import main.java.core.query.QueryType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AverageStatistics extends FinalStatistics {

    private List<FinalStatistics> fullAverageLengthPerModule;

    public AverageStatistics() {
        this.fullAverageLengthPerModule = new ArrayList<>();
    }

    public void updateFinalAverageLengthPerModuleJose(FinalStatistics finalStatistics) {
        fullAverageLengthPerModule.add(finalStatistics);
    }

    public HashMap<String, HashMap<QueryType, Double>> fullAverageLengthPerQueryPerModule() {
        HashMap<String, HashMap<QueryType, Double>> s = new HashMap<>();
        /*int averageFinalStatisticsSize = fullAverageLengthPerModule.size();

        for (FinalStatistics finalStatistics : fullAverageLengthPerModule) {
            finalStatistics.getAverageTimePerQueryTypePerModule().keySet().forEach((k, v) -> s.merge(k, v, Double::sum));
        }

        for (String key : s.keySet()) {
            s.put(key, s.get(key) / averageFinalStatisticsSize);
        }

*/
        return s;
    }

    public HashMap<String, Double> fullAverageLengthPerModule() {
        HashMap<String, Double> finalAverageLengthPerModule = new HashMap<>();
        int averageFinalStatisticsSize = fullAverageLengthPerModule.size();

        for (FinalStatistics finalStatistics : fullAverageLengthPerModule) {
            finalStatistics.getAverageLengthPerModule().forEach((k, v) -> finalAverageLengthPerModule.merge(k, v, Double::sum));
        }

        for (String key : finalAverageLengthPerModule.keySet()) {
            finalAverageLengthPerModule.put(key, finalAverageLengthPerModule.get(key) / averageFinalStatisticsSize);
        }

        return finalAverageLengthPerModule;
    }

    public Double fullDeletedConnections() {
        int averageFinalStatisticsSize = fullAverageLengthPerModule.size();
        int totalDeletedConnections = 0;
        double fullDeletedConnections;

        for (FinalStatistics finalStatistics : fullAverageLengthPerModule) {
            totalDeletedConnections = totalDeletedConnections + finalStatistics.getDeletedConnections();
        }
        fullDeletedConnections = totalDeletedConnections / averageFinalStatisticsSize;
        return fullDeletedConnections;
    }

    public Double fullRejectedConnections() {
        int averageFinalStatisticsSize = fullAverageLengthPerModule.size() + 1;
        int totalRejectedConnections = 0;
        double fullRejectedConnections;

        for (FinalStatistics finalStatistics : fullAverageLengthPerModule) {
            totalRejectedConnections = totalRejectedConnections + finalStatistics.getRejectedConnections();
        }
        fullRejectedConnections = totalRejectedConnections / averageFinalStatisticsSize;
        return fullRejectedConnections;
    }

    public Double fullTotalDiscarted() {
        return (fullDeletedConnections() + fullRejectedConnections());
    }


}
