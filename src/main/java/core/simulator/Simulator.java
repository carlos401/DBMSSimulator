/**
  * UNIVERSIDAD DE COSTA RICA
  * ESCUELA DE CIENCIAS DE LA COMPUTACIÓN E INFORMÁTICA
  * @author Carlos Delgado Rojas
  * @author Jose Valverde Jara
  * @author Esteban Ortega Acuña
  */

package main.java.core.simulator;
import main.java.core.modules.*;
import main.java.core.query.Query;
import main.java.core.random.RandomValueGenerator;
import main.java.core.statistics.AverageStatistics;
import main.java.core.statistics.CurrentStatistics;
import main.java.core.statistics.FinalStatistics;
import main.java.core.statistics.StatisticsController;
import main.java.gui.controllers.ResultsController;
import main.java.log.LogController;

import static main.java.core.simulator.EventType.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * This class is used as a model in the simulator
 */
public class Simulator implements Runnable{

    // For managing in main class
    private StatisticsController statisticsController;

    // The number of simulations to execute
    private int numberOfRepetitions;

    // The max time for each simulation
    private int timePerSimulation;


    // Controls delay
    private Boolean slowMode;

    //save parameters in order to access those
    private int k;
    private int t;
    private int n;
    private int p;
    private int m;

    // The current clock
    private double clock;

    // The list of events
    private List<Event> eventList;

    // Manage all modules in the DBMS
    private RelationalQueryModule relationalQueryManager;
    private TransactionalModule transactionalModule;
    private ProcessManagerModule processManagerModule;
    private ClientManagerModule clientManagerModule;
    private RelationalQueryExecution relationalQueryExecution;

    // Take care with currentStatistics, too important
    public CurrentStatistics currentStatistics;
    public FinalStatistics finalStatistics;
    public AverageStatistics averageStatistics;

    private LogController logController;

    /**
     * @param numberOfRepetitions defines how many times will be executed the simulation
     * @param timePerSimulation   defines how much time has each execution
     * @param slowMode            implements or not a delay of 1 second
     * @param k                   the max number of connections in client manager module
     * @param t                   the time out for each connection in system
     * @param n                   the max number of connections in process manager module
     * @param p                   the max number of connections in transactional module
     * @param m                   the max number of queries in relational query execution module
     */
    public Simulator(int numberOfRepetitions, int timePerSimulation,
                     boolean slowMode, int k, int t, int n, int p, int m) {
        this.currentStatistics = new CurrentStatistics();
        this.finalStatistics = new FinalStatistics();
        this.averageStatistics = new AverageStatistics();

        this.numberOfRepetitions = numberOfRepetitions;
        this.timePerSimulation = timePerSimulation;
        this.slowMode = slowMode;
        this.eventList = new ArrayList<Event>();
        this.k = k;
        this.t = t;
        this.n = n;
        this.p = p;
        this.m = m;

        clientManagerModule = new ClientManagerModule(this, k);
        processManagerModule = new ProcessManagerModule(this, k);
        relationalQueryManager = new RelationalQueryModule(this, n);
        transactionalModule = new TransactionalModule(this, p);
        relationalQueryExecution = new RelationalQueryExecution(this, m);
        this.logController = new LogController();

        this.clock = 0;
        //instances modules
    }

    /**
     * This methods starts the simulation
     * <p>
     * it will be used by the gui controller
     */
    @Override
    public void run() {
            for (int i = 1; i <= this.numberOfRepetitions; i++) {
                this.resetSimulator();
                this.simulate();
                statisticsController.sendFinalStatistics(finalStatistics);
                try{
                    this.logController.logFinalStatistics(finalStatistics);
                } catch (Exception e){
                    e.printStackTrace();
                }
                try{
                    TimeUnit.MILLISECONDS.sleep(70);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    }

    /**
     * Clear variables for new iteration
     */
    private void resetSimulator() {
        this.currentStatistics = new CurrentStatistics();
        this.finalStatistics = new FinalStatistics();

        this.eventList = new ArrayList<>();
        clientManagerModule = new ClientManagerModule(this, k);
        processManagerModule = new ProcessManagerModule(this, k);
        relationalQueryManager = new RelationalQueryModule(this, n);
        transactionalModule = new TransactionalModule(this, p);
        relationalQueryExecution = new RelationalQueryExecution(this, m);
        this.clock = 0;
        //instances modules
    }

    /**
     * Real simulation execution
     */
    private void simulate() {
        //add the initial event
        this.eventList.add(new Event(CLIENT_ARRIVAL,
                0.0, new Query(RandomValueGenerator.generateQueryType())));
        do {
            if (!this.eventList.isEmpty()) {
                this.processNextEvent();
                statisticsController.sendCurrentStatistics(currentStatistics);
                try{
                    this.logController.logCurrentStatistics(currentStatistics);
                } catch (Exception e){
                    e.printStackTrace();
                }
                try{
                    TimeUnit.MILLISECONDS.sleep(70);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(this.slowMode)
                    try{
                        TimeUnit.SECONDS.sleep(1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        } while (this.getClock() <= this.timePerSimulation);
    }

    /**
     * Returns the current clock in system
     * @return the current clock
     */
    public double getClock() {
        return clock;
    }

    /**
     * Allows to set the current clock
     * @param clock the new time
     */
    public void setClock(double clock) {
        this.clock = clock;
    }

    /**
     * Allows add an event to eventlist
     */
    public void addEvent(Event event) {
        eventList.add(event);
    }

    /**
     * Allows execute event every clock increase
     */
    private void processNextEvent() {
        this.eventList.sort(Event::compareTo); //that sorts the list using EventType priority
        if (!this.eventList.isEmpty()) {
            Event event = this.eventList.remove(0); //pop the head
            this.setClock(event.getTime()); //move the clock for next event
            this.currentStatistics.setClock(this.getClock());
            switch (event.getType()) {
                case CLIENT_ARRIVAL:
                    this.clientManagerModule.processArrival(event.getQuery());
                    break;
                case PROCESS_ARRIVAL:
                    this.processManagerModule.processArrival(event.getQuery());
                    break;
                case RELATIONAL_ARRIVAL:
                    this.relationalQueryManager.processArrival(event.getQuery());
                    break;
                case TRANSACTIONAL_ARRIVAL:
                    this.transactionalModule.processArrival(event.getQuery());
                    break;
                case RELATIONAL_EXECUTION_ARRIVAL:
                    this.relationalQueryExecution.processArrival(event.getQuery());
                    break;
                case CLIENT_EXIT:
                    this.clientManagerModule.processExit(event.getQuery());
                    break;
                case PROCESS_EXIT:
                    this.processManagerModule.processExit(event.getQuery());
                    break;
                case RELATIONAL_EXIT:
                    this.relationalQueryManager.processExit(event.getQuery());
                    break;
                case TRANSACTIONAL_EXIT:
                    this.transactionalModule.processExit(event.getQuery());
                    break;
                case RELATIONAL_EXECUTION_EXIT:
                    this.relationalQueryExecution.processExit(event.getQuery());
                    break;
                case TIME_OUT:
                    removeQueryEvents(event.getQuery());
                    this.clientManagerModule.decreaseCurrentConnections();
                    break;

                case FINISHED_RESULT_ARRIVAL:
                    this.clientManagerModule.sendResult(event.getQuery());
                    break;
                case FINISHED_RESULT_EXIT:
                    this.clientManagerModule.finishedQuery(event.getQuery());
                    removeQueryEvents(event.getQuery());
                    break;
            }
        }
    }

    private void removeQueryEvents(Query query){
        Iterator<Event> iterator = this.eventList.iterator();
        List<Event> toRemove = new ArrayList<>();
        while(iterator.hasNext()) {
            Event e = iterator.next();
            if (e.getQuery()==query) {
                toRemove.add(e);
            }
        }
        if (toRemove.size()>0)
            this.eventList.removeAll(toRemove);
    }

    /**
     * @return
     */
    public int getNumberOfRepetitions() {
        return numberOfRepetitions;
    }

    /**
     * @return
     */
    public int getTimePerSimulation() {
        return timePerSimulation;
    }

    /**
     * @return
     */
    public Boolean getSlowMode() {
        return slowMode;
    }

    /**
     * @return
     */
    public String getSlowModeString() {
        return (this.getSlowMode()) ? "activado" : "desactivado";
    }

    /**
     * @return
     */
    public int getK() {
        return k;
    }

    /**
     * @return
     */
    public int getT() {
        return t;
    }

    /**
     * @return
     */
    public int getN() {
        return n;
    }

    /**
     * @return
     */
    public int getP() {
        return p;
    }

    /**
     * @return
     */
    public int getM() {
        return m;
    }

    /**
     *
     * @param resultController
     */
    public void setResultController(ResultsController resultController){
        this.statisticsController = new StatisticsController(resultController);
    }
}