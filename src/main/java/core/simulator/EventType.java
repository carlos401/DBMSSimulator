/*
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Jose Valverde Jara
 * @author Carlos Delgado Rojas
 */

package main.java.core.simulator;

/**
 * This enum defines the different types of Event allowed by the simulator
 */
public enum EventType {
    TIME_OUT,
    CLIENT_ARRIVAL,
    PROCESS_ARRIVAL,
    RELATIONAL_ARRIVAL,
    TRANSACTIONAL_ARRIVAL,
    RELATIONAL_EXECUTION_ARRIVAL,
    CLIENT_EXIT,
    PROCESS_EXIT,
    RELATIONAL_EXIT,
    TRANSACTIONAL_EXIT,
    RELATIONAL_EXECUTION_EXIT,
    FINISHED_RESULT_ARRIVAL,
    FINISHED_RESULT_EXIT
}
