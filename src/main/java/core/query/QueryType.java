/*
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Jose Rodolfo Valverde Jara
 * @author Carlos A. Delgado Rojas
 */

package main.java.core.query;

public enum QueryType {
    SELECT,
    UPDATE,
    JOIN,
    DDL
}


