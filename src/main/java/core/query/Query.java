/*
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Jose Rodolfo Valverde Jara
 * @author Carlos A. Delgado Rojas
 */
package main.java.core.query;

import main.java.core.modules.Module;
import main.java.core.random.RandomValueGenerator;

public class Query implements Comparable<Query> {
    //type of query from the enum class.
    private QueryType queryType;

    //arrival time from query.
    private Double arrivalTime;

    //exit time from query.
    private Double exitTime;

    //timeOut is set false if its in process,else true.
    private Boolean timeOut;

    //Depending on query, blockQuantity is set with weight.
    private Integer blockQuantity;

    //Current module from query.
    private Module currentModule;

    /**
     *
     * @param queryType
     */
    public Query(QueryType queryType) {
        this.queryType = queryType;
        switch (queryType.toString()){
            case "SELECT":
                this.blockQuantity = 1;
                break;
            case "UPDATE":
                this.blockQuantity = 0;
                break;
            case "JOIN":
                this.blockQuantity = RandomValueGenerator.generateUniformTime(1,64).intValue();
                break;
            case "DDL":
                this.blockQuantity = 0;
                break;
        }
    }

    /**
     * get query type from query.
     * @return QueryType
     */
    public QueryType getQueryType() {
        return queryType;
    }

    /**
     * set query type for query.
     * @param queryType
     */
    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }

    /**
     * get arrival time from query.
     * @return arrivalTime
     */
    public Double getArrivalTime() {
        return arrivalTime;
    }

    /**
     * set arrival time for query.
     * @param arrivalTime
     */
    public void setArrivalTime(Double arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * get exit time from query.
     * @return exitTime
     */
    public Double getExitTime() {
        return exitTime;
    }

    /**
     * set ExitTime for query.
     * @param exitTime
     */
    public void setExitTime(Double exitTime) {
        this.exitTime = exitTime;
    }

    /**
     * get if query has timeout or not.
     * @return timeOut
     */
    public Boolean getTimeOut() {
        return timeOut;
    }

    /**
     * set of query has timeout or not.
     * @param timeOut
     */
    public void setTimeOut(Boolean timeOut) {
        this.timeOut = timeOut;
    }

    /**
     * get BlockQuantity from query.
     * @return blockQuantity
     */
    public Integer getBlockQuantity() {
        return blockQuantity;
    }

    /**
     * set BlockQuantity for query.
     * @param blockQuantity
     */
    public void setBlockQuantity(Integer blockQuantity) {
        this.blockQuantity = blockQuantity;
    }

    /**
     * set current module for query.
     * @return currentModule
     */
    public Module getCurrentModule() {
        return currentModule;
    }

    /**
     * get current module for query.
     * @param currentModule
     */
    public void setCurrentModule(Module currentModule) {
        this.currentModule = currentModule;
    }

    /**
     * return true if a query is read only
     * @return true if query is read only, otherwise returns false
     */
    public boolean isReadOnly(){
        if(this.getQueryType() == QueryType.SELECT || this.getQueryType() == QueryType.JOIN)
            return true;
        return false;
    }

    /*
     * value 0 if the argument is equal to this object;
     * a value less than 0 if the argument is an object is greater than this object;
     * a value greater than 0 if the argument is an object is less than this object.
     * */

    @Override
    public int compareTo(Query query) {
        if(query.getArrivalTime() > this.getArrivalTime())
            return -1;
        else if(query.getArrivalTime() > this.getArrivalTime())
            return 1;
        else
            return 0;
    }



}
