/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.modules;
import main.java.core.query.Query;
import main.java.core.simulator.Event;
import main.java.core.simulator.EventType;
import main.java.core.simulator.Simulator;
import java.util.Comparator;
import java.util.PriorityQueue;

import static main.java.core.query.QueryType.*;
import static main.java.core.simulator.EventType.TRANSACTIONAL_EXIT;

/**
 * This class represents the transactional module
 */
public class TransactionalModule extends Module {

    /**
     * This class provides another way to compare Queries
     */
    private class QueryComparator implements  Comparator<Query> {

        /**
         * Compares its two arguments for order.  Returns a negative integer,
         * zero, or a positive integer as the first argument is less than, equal
         * to, or greater than the second.<p>
         * <p>
         * The implementor must ensure that {@code sgn(compare(x, y)) ==
         * -sgn(compare(y, x))} for all {@code x} and {@code y}.  (This
         * implies that {@code compare(x, y)} must throw an exception if and only
         * if {@code compare(y, x)} throws an exception.)<p>
         * <p>
         * The implementor must also ensure that the relation is transitive:
         * {@code ((compare(x, y)>0) && (compare(y, z)>0))} implies
         * {@code compare(x, z)>0}.<p>
         * <p>
         * Finally, the implementor must ensure that {@code compare(x, y)==0}
         * implies that {@code sgn(compare(x, z))==sgn(compare(y, z))} for all
         * {@code z}.<p>
         * <p>
         * It is generally the case, but <i>not</i> strictly required that
         * {@code (compare(x, y)==0) == (x.equals(y))}.  Generally speaking,
         * any comparator that violates this condition should clearly indicate
         * this fact.  The recommended language is "Note: this comparator
         * imposes orderings that are inconsistent with equals."<p>
         * <p>
         * In the foregoing description, the notation
         * {@code sgn(}<i>expression</i>{@code )} designates the mathematical
         * <i>signum</i> function, which is defined to return one of {@code -1},
         * {@code 0}, or {@code 1} according to whether the value of
         * <i>expression</i> is negative, zero, or positive, respectively.
         *
         * @param q1 the first object to be compared.
         * @param q2 the second object to be compared.
         * @return a negative integer, zero, or a positive integer as the
         * first argument is less than, equal to, or greater than the
         * second.
         * @throws NullPointerException if an argument is null and this
         *                              comparator does not permit null arguments
         * @throws ClassCastException   if the arguments' types prevent them from
         *                              being compared by this comparator.
         */
        @Override
        public int compare(Query q1, Query q2) {
            if(q1.getQueryType()==q2.getQueryType()){
                return q1.compareTo(q2); //check time
            } else{
                if (q1.getQueryType()==DDL){
                    return 1;
                } else if(q1.getQueryType()==SELECT) {
                    return -1;
                } else if(q1.getQueryType() == UPDATE){
                    if (q2.getQueryType()!=DDL)
                        return 1;
                    else
                        return -1;
                } else { // (q1.getQueryType()== JOIN)
                    if(q2.getQueryType()!=SELECT)
                        return -1;
                    else
                        return 1;
                }
            }
        }
    }

    /**
     * The constructor
     */
    public TransactionalModule(Simulator sim, int numberOfServers){
        super(sim, numberOfServers);
        this.queriesWaiting = new PriorityQueue<>(new QueryComparator());
    }

    private boolean DDLinProcess = false;

    private int DDLWaiting = 0;

    /**
     * Allows to process an arrival event in event list
     * @param query the query to be processed
     */
    @Override
    public void processArrival(Query query) {
        query.setCurrentModule(this);
        query.setArrivalTime(simulator.getClock());
        if(query.getQueryType()!=DDL && this.queriesInService.size() < numberOfServers
                && !DDLinProcess && DDLWaiting==0){
            query.setExitTime(simulator.getClock()
                    + getServiceTime(query));
            this.queriesInService.add(query);
            this.simulator.currentStatistics.updateLenghtPerModule("Transactional Module", this.queriesWaiting.size() + this.queriesInService.size());

            this.simulator.addEvent(
                    new Event(TRANSACTIONAL_EXIT,query.getExitTime(),query));
        }else {
            if (query.getQueryType() == DDL){
                if (this.queriesInService.size() == 0){
                    query.setExitTime(simulator.getClock()
                            + getServiceTime(query));
                    this.queriesInService.add(query);
                    this.simulator.currentStatistics.updateLenghtPerModule("Transactional Module", this.queriesWaiting.size() + this.queriesInService.size());

                    this.simulator.addEvent(
                            new Event(TRANSACTIONAL_EXIT,query.getExitTime(),query));
                    this.DDLinProcess = true;
                } else {
                    this.DDLWaiting++;
                    this.queriesWaiting.add(query);
                    this.simulator.currentStatistics.updateLenghtPerModule("Transactional Module", this.queriesWaiting.size() + this.queriesInService.size());

                }
            } else {
                this.queriesWaiting.add(query);
                this.simulator.currentStatistics.updateLenghtPerModule("Transactional Module", this.queriesWaiting.size() + this.queriesInService.size());
            }
        }
        this.simulator.finalStatistics.updateAverageLengthPerModule("Transaccional",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     * Allows to process an exit event in event list
     * @param query the query to be processed
     */
    @Override
    public void processExit(Query query){
        if (query.getQueryType() == DDL){
            DDLinProcess = false;
        }
        queriesInService.remove(query);
        if(this.queriesWaiting.size() > 0) {
            Query queryToProcess = queriesWaiting.poll();
            if(queryToProcess.getQueryType() == DDL){
                if(queriesInService.size()!=0){
                    queriesWaiting.add(queryToProcess);
                    this.simulator.currentStatistics.updateLenghtPerModule("Transactional Module", this.queriesWaiting.size() + this.queriesInService.size());

                } else {
                    DDLWaiting--;
                    DDLinProcess = true;
                    queryToProcess.setExitTime(simulator.getClock() + getServiceTime(queryToProcess));
                    this.queriesInService.add(queryToProcess);
                    this.simulator.currentStatistics.updateLenghtPerModule("Transactional Module", this.queriesWaiting.size() + this.queriesInService.size());

                    this.simulator.addEvent(
                            new Event(TRANSACTIONAL_EXIT, queryToProcess.getExitTime(), queryToProcess));
                }
            } else{
                queryToProcess.setExitTime(simulator.getClock() + getServiceTime(queryToProcess));
                this.queriesInService.add(queryToProcess);
                this.simulator.currentStatistics.updateLenghtPerModule("Transactional Module", this.queriesWaiting.size() + this.queriesInService.size());

                this.simulator.addEvent(
                        new Event(TRANSACTIONAL_EXIT, queryToProcess.getExitTime(), queryToProcess));
            }
        }
        query.setArrivalTime(query.getExitTime());
        this.simulator.addEvent(
                new Event(EventType.RELATIONAL_EXECUTION_ARRIVAL,query.getExitTime(),query));

        this.simulator.finalStatistics.updateAverageLengthPerModule("Transaccional",this.queriesInService.size() + this.queriesWaiting.size());
    }


    /**
     * Allows to get random service time for this module
     * @param query the query to be processed
     */
    @Override
    public double getServiceTime(Query query){
        return (numberOfServers * 0.03) + (query.getBlockQuantity()/10);
    }
}
