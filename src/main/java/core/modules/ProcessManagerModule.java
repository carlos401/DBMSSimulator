/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.modules;
import main.java.core.query.Query;
import main.java.core.random.RandomValueGenerator;
import main.java.core.simulator.Event;
import main.java.core.simulator.Simulator;

import java.util.PriorityQueue;

import static main.java.core.simulator.EventType.PROCESS_EXIT;
import static main.java.core.simulator.EventType.RELATIONAL_ARRIVAL;

/**
 * This class represents the process manager module
 */
public class ProcessManagerModule extends Module {

    //
    private final double MEAN = 1;

    //
    private final double VARIANCE = 0.01;

    /**
     * The default constructor
     * @param sim the global simulator
     * @param numberOfServers max connections allowed in this module
     */
    public ProcessManagerModule(Simulator sim, int numberOfServers){
        super(sim, numberOfServers);
        this.queriesWaiting = new PriorityQueue<>();
    }

    /**
     * Allows to process an arrival event in event list
     * @param query the query to be processed
     */
    @Override
    public void processArrival(Query query) {
        query.setCurrentModule(this); //modify query
        query.setArrivalTime(simulator.getClock()); //set arrival time
        if(this.queriesInService.size() < numberOfServers){
            query.setExitTime(simulator.getClock()
                    + getServiceTime(query));
            this.queriesInService.add(query);
            this.simulator.currentStatistics.updateLenghtPerModule("Process Manager Module", this.queriesWaiting.size() + this.queriesInService.size());
            this.simulator.addEvent(
                    new Event(PROCESS_EXIT,query.getExitTime(),query));
        }else{
            this.queriesWaiting.add(query);
            this.simulator.currentStatistics.updateLenghtPerModule("Process Manager Module", this.queriesWaiting.size() + this.queriesInService.size());
        }
        this.simulator.finalStatistics.updateAverageLengthPerModule("Procesado",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     * Allows to process an exit event in event list
     * @param query the query to be processed
     */
    @Override
    public void processExit(Query query){
        queriesInService.remove(query);
        if(this.queriesWaiting.size() > 0){ //there is query waiting
            Query queryToProcess = queriesWaiting.poll(); //get it
            queryToProcess.setExitTime(simulator.getClock()
                    + getServiceTime(queryToProcess));
            this.queriesInService.add(queryToProcess); //put in service
            this.simulator.currentStatistics.updateLenghtPerModule("Process Manager Module", this.queriesWaiting.size() + this.queriesInService.size());
            this.simulator.addEvent(
                    new Event(PROCESS_EXIT,queryToProcess.getExitTime(),queryToProcess));
        }
        this.simulator.addEvent(
                new Event(RELATIONAL_ARRIVAL,query.getExitTime(),query));
        this.simulator.finalStatistics.updateAverageLengthPerModule("Procesado",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     * Allows to get random service time for this module
     * @param query the query to be processed
     */
    @Override
    public double getServiceTime(Query query){
        return RandomValueGenerator.generateNormalTime(MEAN, VARIANCE);
    }
}
