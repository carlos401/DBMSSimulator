/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.modules;

import main.java.core.query.Query;
import main.java.core.query.QueryType;
import main.java.core.simulator.Event;
import main.java.core.simulator.Simulator;

import java.util.PriorityQueue;

import static main.java.core.simulator.EventType.FINISHED_RESULT_ARRIVAL;
import static main.java.core.simulator.EventType.RELATIONAL_EXECUTION_EXIT;

/**
 * This class represents the relational query execution module
 */
public class RelationalQueryExecution extends Module{

    /**
     * The constructor
     */
    public RelationalQueryExecution(Simulator sim, int numberOfServers){
        super(sim, numberOfServers);
        this.queriesWaiting = new PriorityQueue<>();
    }


    /**
     *
     * @param query
     */
    @Override
    public void processArrival(Query query) {
        query.setCurrentModule(this);
        query.setArrivalTime(simulator.getClock());
        if(this.queriesInService.size() < numberOfServers){
            query.setExitTime(simulator.getClock()
                    + getServiceTime(query));
            this.queriesInService.add(query);
            this.simulator.currentStatistics.updateLenghtPerModule("Relational Execution Module", this.queriesWaiting.size() + this.queriesInService.size());

            this.simulator.addEvent(
                    new Event(RELATIONAL_EXECUTION_EXIT,query.getExitTime(),query));
        }else{
            this.queriesWaiting.add(query);
            this.simulator.currentStatistics.updateLenghtPerModule("Relational Execution Module", this.queriesWaiting.size() + this.queriesInService.size());
        }
        this.simulator.finalStatistics.updateAverageLengthPerModule("Relacional",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     *
     * @param query
     */
    @Override
    public void processExit(Query query){
        queriesInService.remove(query);
        if(this.queriesWaiting.size() > 0){
            Query queryToProcess = queriesWaiting.poll();
            queryToProcess.setExitTime(simulator.getClock()
                    + getServiceTime(queryToProcess));
            this.queriesInService.add(queryToProcess);
            this.simulator.currentStatistics.updateLenghtPerModule("Relational Execution Module", this.queriesWaiting.size() + this.queriesInService.size());

            this.simulator.addEvent(
                    new Event(RELATIONAL_EXECUTION_EXIT,queryToProcess.getExitTime(),queryToProcess));
        }
        //time to send result
        this.simulator.addEvent(
                new Event(FINISHED_RESULT_ARRIVAL,query.getExitTime(),query));
        this.simulator.finalStatistics.updateAverageLengthPerModule("Relacional",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     *
     * @param query
     */
    @Override
    public double getServiceTime(Query query){
        double loadTime = 0;
        if(query.getQueryType() == QueryType.DDL)
            loadTime = 1;
        else if(query.getQueryType() == QueryType.UPDATE)
            loadTime = 1;
        return Math.pow(query.getBlockQuantity(), 2) + loadTime;
    }
}
