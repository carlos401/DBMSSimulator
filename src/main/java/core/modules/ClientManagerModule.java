/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.modules;
import main.java.core.query.Query;
import main.java.core.random.RandomValueGenerator;
import main.java.core.simulator.Event;
import main.java.core.simulator.Simulator;

import java.util.PriorityQueue;
import java.util.Queue;

import static main.java.core.simulator.EventType.*;

/**
 * This class represents the client manager module
 */
public class ClientManagerModule extends Module {

    // controls total queries in system
    private int currentConnections;

    private Queue<Query> sendingResultsQueue;

    /**
     * The default constructor
     * @param sim the global simulator
     * @param numberOfServers max number of connections in system
     */
    public ClientManagerModule(Simulator sim, int numberOfServers){
        super(sim, numberOfServers);
        this.queriesWaiting = new PriorityQueue<>();
        this.currentConnections = 0;
        sendingResultsQueue = new PriorityQueue<>();
    }

    // lambda parameter
    private final double QUERY_MEAN = 0.5;

    /**
     * Allows to process an arrival event in event list
     * @param query the query to be processed
     */
    @Override
    public void processArrival(Query query) {
        query.setCurrentModule(this); //modify query
        query.setArrivalTime(simulator.getClock()); //set arrival time
        if(this.currentConnections < numberOfServers){ //processes query immediately
            this.currentConnections++;
            query.setExitTime(simulator.getClock());//time in service = 0;
            this.queriesInService.add(query);
            this.simulator.currentStatistics.updateLenghtPerModule("Client Manager Module", this.queriesInService.size());
            this.simulator.addEvent(
                    new Event(
                            CLIENT_EXIT,query.getExitTime(),query)); //generates exit

            double timeOut = simulator.getClock() + simulator.getT();
            this.simulator.addEvent(
                    new Event(TIME_OUT, timeOut, query)); //generates timeOut
        }else{
            this.simulator.currentStatistics.increaseRejectedConnections();
            //++rejectedConnections; STATISTICS
        }
        double arrivalTime =
                simulator.getClock() + RandomValueGenerator.generateExponentialTime(QUERY_MEAN);
        this.simulator.addEvent(
                new Event(CLIENT_ARRIVAL, arrivalTime,
                        new Query(RandomValueGenerator.generateQueryType())));
        this.simulator.finalStatistics.updateAverageLengthPerModule("Clientes",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     * Allows to process an exit event in event list
     * @param query the query to be processed
     */
    @Override
    public void processExit(Query query){
        queriesInService.remove(query);
        this.simulator.addEvent(
                new Event(PROCESS_ARRIVAL,query.getExitTime(),query));
        this.simulator.finalStatistics.updateAverageLengthPerModule("Clientes",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     * Allows finished the process
     * @param query
     */
    public void finishedQuery(Query query){
        sendingResultsQueue.remove(query);
        decreaseCurrentConnections();
        this.simulator.finalStatistics.updateAverageLengthPerModule("Clientes",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     *
     * @param query
     */
    public void sendResult(Query query){
        sendingResultsQueue.add(query);
        this.simulator.addEvent(
                new Event(FINISHED_RESULT_EXIT,this.simulator.getClock()
                        + query.getBlockQuantity()/64, query));
    }

    public void decreaseCurrentConnections(){
        this.currentConnections--;
        this.simulator.currentStatistics.increaseDeletedConnections();
    }
}
