/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.modules;
import main.java.core.query.Query;
import main.java.core.random.RandomValueGenerator;
import main.java.core.simulator.Event;
import main.java.core.simulator.Simulator;

import java.util.PriorityQueue;

import static main.java.core.simulator.EventType.*;

/**
 * This class represents the relational query module
 */
public class RelationalQueryModule extends Module{

    /**
     * The constructor
     */
    public RelationalQueryModule(Simulator sim, int numberOfServers){
        super(sim, numberOfServers);
        this.queriesWaiting = new PriorityQueue<>();
    }

    /**
     * Allows to process an arrival event in event list
     * @param query the query to be processed
     */
    @Override
    public void processArrival(Query query) {
        query.setCurrentModule(this); //modify query
        query.setArrivalTime(simulator.getClock()); //set arrival time
        if(this.queriesInService.size() < numberOfServers){
            query.setExitTime(simulator.getClock()
                    + getServiceTime(query));
            this.queriesInService.add(query);
            this.simulator.currentStatistics.updateLenghtPerModule("Relational Manager Module", this.queriesWaiting.size() + this.queriesInService.size());

            this.simulator.addEvent(
                    new Event(RELATIONAL_EXIT,query.getExitTime(),query));
        }else{
            this.queriesWaiting.add(query);
            this.simulator.currentStatistics.updateLenghtPerModule("Relational Manager Module", this.queriesWaiting.size() + this.queriesInService.size());
        }
        this.simulator.finalStatistics.updateAverageLengthPerModule("Relacional",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     * Allows to process an exit event in event list
     * @param query the query to be processed
     */
    @Override
    public void processExit(Query query){
        queriesInService.remove(query);
        if(this.queriesWaiting.size() > 0){ //there is query waiting
            Query queryToProcess = queriesWaiting.poll(); //get it
            queryToProcess.setExitTime(simulator.getClock()
                    + getServiceTime(queryToProcess));
            this.queriesInService.add(queryToProcess); //put in service
            this.simulator.currentStatistics.updateLenghtPerModule("Relational Manager Module", this.queriesWaiting.size() + this.queriesInService.size());

            this.simulator.addEvent(
                    new Event(RELATIONAL_EXIT,queryToProcess.getExitTime(),queryToProcess));
        }
        this.simulator.addEvent(
                new Event(TRANSACTIONAL_ARRIVAL,query.getExitTime(),query));
        this.simulator.finalStatistics.updateAverageLengthPerModule("Relacional",this.queriesInService.size() + this.queriesWaiting.size());
    }

    /**
     * Allows to get random service time for this module
     * @param query the query to be processed
     */
    @Override
    public double getServiceTime(Query query){
        //mean given by req
        double QUERY_MEAN = (1/(0.7));
        return 1/10
                + RandomValueGenerator.generateUniformTime(0, 1)
                + RandomValueGenerator.generateUniformTime(0, 2)
                + RandomValueGenerator.generateExponentialTime(QUERY_MEAN)
                + ( query.isReadOnly() ? 0.1 : 0.25);
    }
}
