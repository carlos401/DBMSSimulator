/**
 * UNIVERSIDAD DE COSTA RICA
 * ESCUELA DE CIENCIAS DE LA COMPUTACION E INFORMATICA
 * @author Carlos Delgado Rojas
 * @author Jose Valverde Jara
 * @author Esteban Ortega Acuña
 */

package main.java.core.modules;

import main.java.core.query.Query;
import main.java.core.query.QueryType;
import main.java.core.random.RandomValueGenerator;
import main.java.core.simulator.Event;
import main.java.core.simulator.EventType;
import main.java.core.simulator.Simulator;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * This class is an abstraction of modules
 */
public class Module {

    // current number of servers, defined by the user
    protected int numberOfServers;

    // number of queries waiting
    protected Queue<Query> queriesWaiting;

    // number of queries in service
    protected List<Query> queriesInService;

    Simulator simulator;

    /**
     * The constructor
     */
    public Module(Simulator simulator, int numberOfServers) {
        this.numberOfServers = numberOfServers;
        //this.queriesWaiting = new PriorityQueue<>();
        this.queriesInService = new ArrayList<>();
        this.simulator = simulator;
    }

    public double getServiceTime(Query query){return 0;}


    public void processArrival(Query query) {

    }

    /**
     *
     * @param query
     */
    public void processExit(Query query){


    }

    /**
     * @param query
     */
    public void processTimeOut(Query query) {

    }



    /**
     * @return
     */
    public int getNumberOfServers() {

        return numberOfServers;
    }


    /**
     * @return
     */
    public Queue<Query> getQueriesWaiting() {

        return queriesWaiting;
    }

    /**
     * @return
     */
    public List<Query> getQueriesInService() {

        return queriesInService;
    }




}

