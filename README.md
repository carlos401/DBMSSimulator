# Project requirements

Requirements available [here](https://gitlab.com/carlos401/DBMSSimulator/blob/master/doc/requirements.pdf)

## Usefull documentation

* Read about JavaFx [here](https://www.java.com/es/download/faq/javafx.xml)
* Java Scene Builder [here](http://www.oracle.com/technetwork/java/javase/downloads/javafxscenebuilder-info-2157684.html) 

---

# Design

* UML design available [here](https://gitlab.com/carlos401/DBMSSimulator/blob/master/doc/umldesign.pdf)
* GUI proposal available [here](https://goo.gl/x5zwGV)

---

# License

 DBMSSimulator
    Copyright (C) 2018  Carlos Delgado

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Contact information: carlos.delgadorojas@ucr.ac.cr